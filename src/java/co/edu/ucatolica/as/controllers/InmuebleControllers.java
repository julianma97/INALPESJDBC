/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucatolica.as.controllers;

import co.edu.ucatolica.as.DAOs.InmuebleDao;
import co.edu.ucatolica.as.DAOs.PersonaDAO;
import co.edu.ucatolica.as.DTOs.Inmueble;
import co.edu.ucatolica.as.DTOs.Persona;
import co.edu.ucatolica.as.bds.MySqlDataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

@Controller
@RequestMapping("/")

public class InmuebleControllers {
    @RequestMapping(method = RequestMethod.GET, value = "InmuebleCrear.htm")
    public String processSubmit(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {

        System.out.println("InmuebleCrear");
        model.put("mensajePersona", "Pase por el controller de Persona:::"+req.getParameter("nombre"));
        return "crear_inmueble";
    }    
  
@RequestMapping(method = RequestMethod.POST, value = "inmuebleRegistrar.htm")
    public String processSubmit1(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {

        InmuebleDao pDao = new InmuebleDao();
            
        Logger.getLogger(PersonaControllers.class.getName()).log(Level.INFO, "Ejecutando Registrar inmueble");

       
        String id = req.getParameter("IdInmueble");
        String nombre = req.getParameter("Nombre_Inmueble");
        String tipo = req.getParameter("Tipop");
        String sucursales = req.getParameter("Sucursales");

        
        Inmueble p = new Inmueble();
       
        p.setId(Integer.parseInt(id));
        p.setNombre_inmueble(nombre);
        p.setSucursales_idsucursales(sucursales);
        p.setTipo_inmueble_idtipo_inmueble(tipo);
            
        boolean insert = pDao.crearInmueble(p, MySqlDataSource.getConexionBD());

        Logger.getLogger(PersonaControllers.class.getName()).log(Level.SEVERE, null, "Registrar + " + id + "-" + insert);
        
        if (insert)
            model.put("mensaje", "El registro fue creado satisfactoriamente!!!");
        else
            model.put("mensaje", "El registro NO fue creado, consulte con el administrador...");
        
        return "crear_inmueble";
    }     
    
    @RequestMapping(method = RequestMethod.GET, value = "ConsultarInmueble.htm")
    public String processSubmit2(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {      
        Logger.getLogger(PersonaControllers.class.getName()).log(Level.INFO, "Ejecutando processSubmit2...");
        return "consultar_inmueble";
    } 
    
@RequestMapping(method = RequestMethod.POST, value = "InmuebleConsultarForm.htm")
    public String processSubmit3(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {

        InmuebleDao pDao = new InmuebleDao();
            
        Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando Consultar inmueble ");

       
        String id = req.getParameter("IdInmueble");
        String nombre1 = req.getParameter("Nombre_Inmueble");
        
        Inmueble p = new Inmueble();
        
        p.setId(Integer.parseInt(id));
        p.setNombre_inmueble(nombre1);
            
        List<Inmueble> datos = pDao.consultarInmueble(p, MySqlDataSource.getConexionBD());

        Logger.getLogger(PersonaControllers.class.getName()).log(Level.SEVERE, null, "Consultar + " + id + "-" + datos.size());
        
        model.put("listaInmueble", datos);
        if (datos.size() > 0)
            model.put("mensaje", "La consulta se realizo satisfactoriamente!!!" + datos.size());
        else
            model.put("mensaje", "La consulta NO tiene resultados...");
        
        return "consultar_inmueble";
    }     
    
@RequestMapping(method = RequestMethod.GET, value = "EditarInmueble.htm")
    public String processSubmit4(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {      
        Logger.getLogger(PersonaControllers.class.getName()).log(Level.INFO, "Ejecutando processSubmit4...");
        return "editar_inmueble";
    } 
    
@RequestMapping(method = RequestMethod.POST, value = "InmuebleEditarForm1.htm")
    public String processSubmit5(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {

        InmuebleDao pDao = new InmuebleDao();
            
        Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Ejecutando inmueble...");

        
        String id = req.getParameter("IdInmueble");
        String nombre1 = req.getParameter("Nombre_Inmueble");
        
        Inmueble p = new Inmueble();
        
        p.setId(Integer.parseInt(id));
        p.setNombre_inmueble(nombre1);
            
        List<Inmueble> datos = pDao.consultarInmueble(p, MySqlDataSource.getConexionBD());

        Logger.getLogger(PersonaControllers.class.getName()).log(Level.SEVERE, null, "Consultar + " + id + "-" + datos.size());
        
        model.put("listaInmueble", datos);
        
        
        return "editar_inmueble";
        
    }    
    
@RequestMapping(method = RequestMethod.POST, value = "InmuebleEditarForm2.htm")
    public String processSubmit6(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {

        InmuebleDao pDao = new InmuebleDao();
            
        Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Editando inmueble...");

        String id = req.getParameter("IdInmueble");
        String nombre1 = req.getParameter("Nombre_Inmueble");
        String Tipo_p = req.getParameter("Tipo_inmueble");
        String Sucursal = req.getParameter("Sucursal");
        
        Logger.getLogger(PersonaDAO.class.getName()).log(Level.INFO, "Id Inmueble: " + id);
        
        Inmueble p = new Inmueble();
        p.setId(Integer.parseInt(id));
        p.setNombre_inmueble(nombre1);
        p.setTipo_inmueble_idtipo_inmueble(Tipo_p);
        p.setSucursales_idsucursales(Sucursal);
            
        boolean res = pDao.editarInmueble(p, MySqlDataSource.getConexionBD());                         
        
        if (res)
            model.put("mensaje", "Se edito satisfactoriamente!!!");
        else
            model.put("mensaje", "NO se guardaron los cambios...");
        
        return "editar_inmueble";
        
    }    
}
