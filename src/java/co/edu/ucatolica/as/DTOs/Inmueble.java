/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.ucatolica.as.DTOs;

/**
 *
 * @author nixoduaa
 */
public class Inmueble {

            int id = 0;
            String nombre_inmueble = null;
            String Tipo_inmueble_idtipo_inmueble = null;
            String Sucursales_idsucursales = null;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre_inmueble() {
        return nombre_inmueble;
    }

    public void setNombre_inmueble(String nombre_inmueble) {
        this.nombre_inmueble = nombre_inmueble;
    }

    public String getTipo_inmueble_idtipo_inmueble() {
        return Tipo_inmueble_idtipo_inmueble;
    }

    public void setTipo_inmueble_idtipo_inmueble(String Tipo_inmueble_idtipo_inmueble) {
        this.Tipo_inmueble_idtipo_inmueble = Tipo_inmueble_idtipo_inmueble;
    }

    public String getSucursales_idsucursales() {
        return Sucursales_idsucursales;
    }

    public void setSucursales_idsucursales(String Sucursales_idsucursales) {
        this.Sucursales_idsucursales = Sucursales_idsucursales;
    }
    
            
}
